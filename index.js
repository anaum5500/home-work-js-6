function Worker (name,surname,rate,days){
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
    this.getSallary = function (){
        return this.rate * this.days
    }
}
const workerOne = new Worker ("Ivan","Ivanov",50,22);
document.write(`Имя: ${workerOne.name}</br>  Фамилия: ${workerOne.surname}</br> Зарплата: ${workerOne.getSallary()}<hr>`);
const workerTwo = new Worker ("Petr","Petrov",80,35);
document.write(`Имя: ${workerTwo.name}</br>  Фамилия: ${workerTwo.surname}</br> Зарплата: ${workerTwo.getSallary()}<hr>`);
const workerThree = new Worker ("Pavel","Pavlov",43.5,18);
document.write(`Имя: ${workerThree.name}</br>  Фамилия: ${workerThree.surname}</br> Зарплата: ${workerThree.getSallary()}<hr>`);


function MyString (str){
    this.str = str;
    this.reverseString = function(){
        return this.str.split('').reverse().join('');
    }
    this.firstLetterToUp = function(){
        return this.str.charAt(0).toUpperCase() + this.str.slice(1);
    }
    this.allFirstLetterUp = function(){
        return this.str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
    }
    

}
const stringForChange = "this good string";
const changedString = new MyString(stringForChange);
document.write(`Оригинальная строка:  ${stringForChange} </br> Измененная строка:   ${changedString.reverseString()}<hr>`)
document.write(`Оригинальная строка:  ${stringForChange} </br> Измененная строка:   ${changedString.firstLetterToUp()}<hr>`)
document.write(`Оригинальная строка:  ${stringForChange} </br> Измененная строка:   ${changedString.allFirstLetterUp()}<hr>`)